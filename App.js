import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Animated,
} from "react-native";

export default class MyComponent extends Component {
  state = {
    ready: false,
    blue: new Animated.Value(0),
    yellow: new Animated.Value(0),
    green: new Animated.Value(0)
  };

  _start = () => {
    return Animated.parallel([
      Animated.timing(this.state.blue, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true
      }),
      Animated.timing(this.state.green, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true
      }),
      Animated.timing(this.state.yellow, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true
      })
    ]).start();
  };

  componentDidMount(){
    this._start();
  }

  render() {
    let { yellow, green, blue } = this.state;
    return (
      <View style={styles.container}>

        <Animated.View
          style={{
            transform: [
              {
                translateX: green.interpolate({
                  inputRange: [0, 1],
                  outputRange: [-2000, -30]
                })
              }
            ],
            marginTop: 250,
            height: 120,
            width: 60,
            backgroundColor: "#AFFF33"
          }}
        >
        </Animated.View>

        <Animated.View
          style={{
            transform: [
              {
                translateY: blue.interpolate({
                  inputRange: [0, 1],
                  outputRange: [-1200, -120]
                })
              }
            ],
            marginLeft: 60,
            height: 60,
            width: 60,
            backgroundColor: "#33F2FF",
            marginTop: 0
          }}
        >
        </Animated.View>

        <Animated.View
          style={{
            transform: [
              {
                translateY: yellow.interpolate({
                  inputRange: [0, 1],
                  outputRange: [1200, -120]
                })
              }
            ],
            marginLeft: 60,
            height: 60,
            width: 60,
            backgroundColor: "#FFE733",
          }}
        >
        </Animated.View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FEDD",
    alignItems: "center"
  }
});